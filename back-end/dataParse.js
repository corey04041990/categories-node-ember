class DataParse {
  constructor() {
    this.data = []
  }

  toJSON(data) {
    return JSON.stringify(data)
  }
}

module.exports = function () {
  return new DataParse()
}
